'use strict';

const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
var admin = require("firebase-admin");
var serviceAccount = require("./json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://maeprojectfamily.firebaseio.com"
});
var registrationToken = "f8dXgGBWA3c:APA91bFiHZ1bQ8b8pBftJarB5If2BDo6eTBFFAXWlxmAuLFYn219-qqTKb8xoLFTKBeaHKzScgF4nuQwk2L0pKrzDjvQDL6W0pMY3LXgd8q1SWIva3ETktBDy6djW-ZyI3TKreMH4Ngf";
console.log(registrationToken);

const PORT = process.env.PORT || 6060;
const INDEX = path.join(__dirname, 'index.html');

const server = express()
    .use((req, res) => res.sendFile(INDEX))
    .listen(PORT, () => console.log(`Listening on ${ PORT }`));

const io = socketIO(server);

let patientPhone = null;
let familyPhone = null;


io.on('connection', (socket) => {
    // Server / Client connection
    console.log('Client connected');

    socket.on('disconnect', () => {
        if (socket.id === familyPhone) {
            familyPhone = null;
            if (patientPhone) io.to(`${patientPhone}`).emit('disconnectedFamily', 'Familly disconnected');
        }
        if (socket.id === patientPhone) {
            patientPhone = null;
            if (familyPhone) io.to(`${familyPhone}`).emit('disconnectedPatient', 'Patient disconnected');
        }
    });


    socket.on('connectedPatient', () => {
        patientPhone = socket.id;
        if (patientPhone) io.to(`${patientPhone}`).emit('connectedPatientCallback', 'Patient connected');
        if (patientPhone && familyPhone) io.emit('allMembersConnected', 'everyone connected');
    });

    socket.on('connectedFamily', () => {
        familyPhone = socket.id;
        if (familyPhone) io.to(`${familyPhone}`).emit('connectedFamilyCallback', 'Family connected');
        if (patientPhone && familyPhone) io.emit('allMembersConnected', 'everyone connected');
    });

    // Message
    socket.on('sendMsgToPatient', (data) => {
        if (patientPhone) io.to(`${patientPhone}`).emit('sendMsgToPatient', data);
    });
    socket.on('sendMsgToFamily', (data) => {
        if (familyPhone) io.to(`${familyPhone}`).emit('sendMsgToFamily', data);
    });

    // Steps
    socket.on("nextStep", function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('nextStep', data);
        if (familyPhone) io.to(`${familyPhone}`).emit('nextStep', data);
    });


    // Video preview
    socket.on('video', function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('toVideo', data)
    });


    // Invitations
    socket.on('invitationSentToPatient', function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('invitationReceived', data)
    });

    socket.on('invitationSentToFamily', function (data) {
        var payload = {
            notification: {
                title: 'Agnès vous appelle',
                body: 'Êtes vous disponible ?',
            },
            apns: {
                payload: {
                    aps: {
                        "badge": 1,
                        "sound": "default",
                        "content-available": 1,
                        "mutable-content": 1,
                        "category": "myCategory"
                    },
                },
            },
            token: registrationToken
        };
        var options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
        };
        admin.messaging().send(payload)
            .then(function (response) {
                console.log("Successfully sent message:", response);
            })
            .catch(function (error) {
                console.log("Error sending message:", error);
            });

        if (familyPhone) io.to(`${familyPhone}`).emit('invitationReceived', data)
    });

    socket.on('familyAnswered', function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('invitationAnswered', data)
    });
    socket.on('patientAnswered', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('invitationAnswered', data)
    });


    // Connections
    socket.on('bleConnected', function (data) {

        if (patientPhone) io.to(`${patientPhone}`).emit('bleConnected', data)
    });

    socket.on('sparkConnected', function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('sparkConnected', data)

    });

    socket.on('setupDone', function () {
        if (patientPhone) io.to(`${patientPhone}`).emit('nextStep', "2");
        if (familyPhone) io.to(`${familyPhone}`).emit('nextStep', "2");
    });


    // Ready for Take off
    socket.on('familyReadyToTakeOff', function () {
        if (patientPhone) io.to(`${patientPhone}`).emit('nextStep', "3");
        if (familyPhone) io.to(`${familyPhone}`).emit('nextStep', "3");
    });

    socket.on('patientReadyToTakeOff', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('patientReadyToTakeOff', data)
    });

    socket.on('endExperience', function () {
        if (familyPhone) io.to(`${familyPhone}`).emit('endExperience');
        //if (patientPhone) io.to(`${patientPhone}`).emit('endExperience');
    });

    socket.on('droneLanded', function () {
        // if (familyPhone) io.to(`${familyPhone}`).emit('endExperience');
        if (patientPhone) io.to(`${patientPhone}`).emit('droneLanded');
    });


    // Pilotage
    socket.on('actionSparkEvent', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('actionSparkEvent', data)
    });

    socket.on('actionSparkDirectionHorizontal', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('actionSparkDirectionHorizontal', data)
    });

    socket.on('actionSparkRotation', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('actionSparkRotation', data)
    });

    socket.on('sparkMotorsOff', function () {
        if (patientPhone) io.to(`${patientPhone}`).emit('sparkMotorsOff');
    });

    socket.on('familyPiloting', function(){
        if (patientPhone) io.to(`${patientPhone}`).emit('familyPiloting');
    });

    socket.on('familyWatching', function(){
        if (patientPhone) io.to(`${patientPhone}`).emit('familyWatching');
    });

    socket.on('sendGimbalValue', function(data){
        if (familyPhone) io.to(`${familyPhone}`).emit('sendGimbalValue', data);
    });


    // Emotions
    socket.on('actionSparkFeeling', function (data) {
        if (familyPhone) io.to(`${familyPhone}`).emit('actionSparkFeeling', data);
    });
    socket.on('actionFeeling', function (data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('actionSparkFeeling', data);
    });
    socket.on('feelingFinished', () => {
        if (patientPhone) io.to(`${patientPhone}`).emit('feelingFinished');
    });


    // Battery

    socket.on('sendLevelBattery', function(data) {
        if (patientPhone) io.to(`${patientPhone}`).emit('sendLevelBattery', data);
    });

});
